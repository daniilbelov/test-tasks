<?php

class NullData
{
    protected $data = [];

    public function start() {
        $data = json_decode(file_get_contents("./chart.json"));

        foreach ($data as $item) $this->filter_array($item);

        $fp = fopen('chart_result.json', 'w');
        fwrite($fp, json_encode($this->data));
        fclose($fp);
    }

    protected function filter_array($arr) {
        $data = [];
        $to_null = [];

        foreach ($arr as $number) {
            if ($number == 100) $to_null[] = $number;
            else {
                if (count($to_null) > 3) $to_null = $this->nullify($to_null);

                $data = array_merge($data, $to_null);
                $data[] = $number;
                $to_null = [];
            }
        }

        if (count($to_null) > 3) $to_null = $this->nullify($to_null);

        $data = array_merge($data, $to_null);

        $this->data[] = $data;
    }

    protected function nullify($arr) {
        return array_map(function () {
            return null;
        }, $arr);
    }
}

$filter = new NullData();
$filter->start();