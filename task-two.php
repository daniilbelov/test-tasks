<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetTitle('Главная');
?> 

<?

function dd($dump = '') {
    echo '<pre>';
    var_dump($dump);
    echo '</pre>';
    die();
}

CModule::IncludeModule("catalog");
CModule::IncludeModule("iblock");
CModule::IncludeModule("pirce");

class Product
{
    protected $catalog, $iblock, $product, $price, $main_catalog;

    public function __construct() {
        $this->catalog = new CCatalog();
        $this->iblock = new CIBlockElement();
        $this->product = new CCatalogProduct();
        $this->price = new CPrice();
    }

    public function add($iblock_id, $product) {
        $this->main_catalog = $this->catalog->GetByID($iblock_id);
        $product_id = $this->create($product);

        foreach ($product['offers'] as $offer) $this->create($offer, $product_id);
    }

    protected function create($product, $offer = false) {
        $fields = [
            'NAME' => $product['name'],
            'IBLOCK_ID' => $offer ? $this->main_catalog['OFFERS_IBLOCK_ID'] : $this->main_catalog['IBLOCK_ID'],
            'CODE' => $this->slug($product['name']),
            'ACTIVE' => 'Y',
            'PROPERTY_VALUES' => []
        ];

        if ($offer) $fields['PROPERTY_VALUES'] = [$this->main_catalog['OFFERS_PROPERTY_ID'] => $offer];
        $fields['PROPERTY_VALUES'] += $product['props'];

        $product_id = $this->iblock->Add($fields);
        $this->product->Add(['ID' => $product_id]);
        $this->price->SetBasePrice($product_id, $product['price'], 'RUB');

        return $product_id;
    }

    protected function slug($string) {
        $cyr = [
            'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
            'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
        ];
        $lat = [
            'a', 'b', 'v', 'g', 'd', 'e', 'io', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p',
            'r', 's', 't', 'u', 'f', 'h', 'ts', 'ch', 'sh', 'sht', '', 'i', '', 'e', 'yu', 'ya'
        ];

        return str_replace(' ', '-', str_replace($cyr, $lat, strtolower($string)));
    }
}

$test = new Product();
$test->add(1, [
    'name' => 'Новый Товар', 
    'price' => 1000, 
    'props' => [],
    'offers' => [
        [
            'name' => 'Новый Товар 2',
            'price' => 999,
            'props' => []
        ],
        [
            'name' => 'Новый Товар 3',
            'price' => 1001,
            'props' => []
        ]
    ]]);

?>

<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>